FROM node

#RUN ["npm", "install", "--global", "npm@10.4.0"]
#RUN ["npm", "install", "--global", "yarn"]

WORKDIR /app
COPY ./yarn.lock .
COPY ./package.json .

RUN ["yarn", "install"]

COPY . .

RUN ["yarn", "build"]
RUN ["npm", "install", "--global", "serve"]

CMD ["serve", "-p", "8080", "dist"]