import axios from "axios";
import ClientCommons from "@/client/ClientCommons";


export default class AccountClient {

    static async getProfileByLinkedInUri(linkedInUri) {
        linkedInUri = linkedInUri.replace("http://", "").replace("https://", "")
        if (linkedInUri.endsWith("/")) linkedInUri = linkedInUri.substring(0, linkedInUri.length - 1);
        const url = ClientCommons.getBaseUrl() + "/api/account";
        return (await axios.get(url, {
            headers: {
                ...ClientCommons.getHeaders()
            },
            params: {
                url: linkedInUri
            }
        })).data
    }

    static async updateWalletDetails(linkedInUri, privateKey, address) {
        linkedInUri = linkedInUri.replace("http://", "").replace("https://", "")
        if (linkedInUri.endsWith("/")) linkedInUri = linkedInUri.substring(0, linkedInUri.length - 1);
        const url = ClientCommons.getBaseUrl() + "/api/account/wallet"
        await axios.post(url, {
            linkedInUri,
            privateKey,
            address
        }, {
            headers: {
                ...ClientCommons.getHeaders()
            }
        });
    }

}