

export default class ClientCommons {

    static getBaseUrl() {
        let url = new URL(window.location.origin);
        url.port = "8080"
        url = url.toString()
        url = url.substring(0, url.length - 1)
        console.log("The url is: " + url)
        return url;
    }

    static getHeaders() {
        return {
            'Content-Type': 'application/json'
        }
    }

}