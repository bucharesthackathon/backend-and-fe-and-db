import axios from "axios";
import ClientCommons from "@/client/ClientCommons";

export default class SearchClient {

    static async searchByJobDescription(jobDescription) {
        const url = ClientCommons.getBaseUrl() + "/api/profiles/closest/description"
        return (await axios.post(url, { jobDescription }, {
            headers: {
                ...ClientCommons.getHeaders()
            }
        })).data;
    }

    static async searchByLinkedInUrl(linkedInUri) {
        linkedInUri = linkedInUri.replace("http://", "").replace("https://", "")
        if (linkedInUri.endsWith("/")) {
            linkedInUri = linkedInUri.substring(0, linkedInUri.length - 1);
        }
        const url = ClientCommons.getBaseUrl() + "/api/profiles/closest/profile"
        return (await axios.post(url, { linkedInUri }, {
            headers: {
                ...ClientCommons.getHeaders()
            }
        })).data;
    }

}