import { createRouter, createWebHistory } from 'vue-router'
import SearchByJobDescription from "@/views/SearchByJobDescription.vue";
import SearchBySimilarProfiles from "@/views/SearchBySimilarProfiles.vue";
import AccountSettings from "@/views/AccountSettings.vue";

const routes = [
  {
    path: '/',
    name: 'home',
    component: SearchByJobDescription
  },
  {
    path: '/jobDescriptionSearch',
    name: 'jobDescriptionSearch',
    component: SearchByJobDescription
  },
  {
    path: '/similarProfiles',
    name: 'similarProfiles',
    component: SearchBySimilarProfiles
  },
  {
    path: '/settings',
    name: 'settings',
    component: AccountSettings
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
